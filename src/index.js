const express = require("express");
const app = express();
app.use(express.json())
const cors = require("cors");
const rootRoute = require("./routes/rootRoute");
app.use(cors())
const port = 8080;
 
  app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
  })

app.use("/api",rootRoute)