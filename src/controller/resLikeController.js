const initModels = require("../models/init-models")
const sequelize = require("../models/index")
const model = initModels(sequelize);

const getLikeRes = async (req,res) => {
    let data = await model.restaurant.findAll({
        include: ["user_id_users"],
    })
    if(data){
        res.status(200).send(data);
    }else{
        res.status(400).send("Không tìm thấy dữ liệu");
    }
    
}
const getLikeResId = async (req,res) => {
    let id = req.params.id;
    let data = await model.restaurant.findOne({
        where: {
            res_id : id,
        },
        include: ["user_id_users"]
    })
    if (data){
        res.status(200).send(data);
    }else{
        res.status(400).send("Không tìm thấy nhà hàng!")
    }
}
const postLikeRes = async (req,res) => {
    const data = req.body;
    let index = await model.like_res.findOne({
        where: {
            res_id: data.res_id,
            user_id: data.user_id
        }
    });
    if(index !== null){
        res.status(400).send("Bạn đã like nhà hàng rồi!")
    }
    else{
        await model.like_res.create({
            res_id: data.res_id,
            user_id: data.user_id,
            date_like: data.dateLike
        })
        res.status(200).send("Like nhà hàng thành công!")
    }
}
const unLikeRes = async (req,res) => {
    const data = req.body;
    let index = await model.like_res.findOne({
        where: {
            res_id: data.res_id,
            user_id: data.user_id
        }
    });
    console.log(index);
    if(index == null){
        res.status(400).send("ID user hoặc nhà hàng không chính xác!")
    }else{
        await model.like_res.destroy({
            where: {
                res_id: data.res_id,
                user_id: data.user_id
            }
        })
        res.status(200).send("Unlike Thành Công!")
    }
}
module.exports = {getLikeRes,getLikeResId,postLikeRes,unLikeRes}