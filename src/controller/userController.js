const initModels = require("../models/init-models")
const sequelize = require("../models/index")
const model = initModels(sequelize);

const getLikeUser = async (req,res) => {
    let data = await model.user.findAll({
        include: ["res_id_restaurants"],
    })
    if(data){
        res.status(200).send(data);
    }else{
        res.status(400).send("Không tìm thấy dữ liệu");
    }
}
const getLikeUserId = async (req,res) => {
    let id = req.params.id;
    let data = await model.user.findOne({
        where: {
            user_id : id,
        },
        include: ["res_id_restaurants"]
    })
    if (data){
        res.status(200).send(data);
    }else{
        res.status(400).send("Không tìm thấy user!")
    }
}
const getRateUser = async (req,res) => {
    let data = await model.user.findAll({
        include: ["res_id_restaurant_rate_res"],
    })
    if(data){
        res.status(200).send(data);
    }else{
        res.status(400).send("Không tìm thấy dữ liệu");
    }
}
const getRateUserId = async (req,res) => {
    let id = req.params.id;
    let data = await model.user.findOne({
        where: {
            user_id : id,
        },
        include: ["res_id_restaurant_rate_res"]
    })
    if (data){
        res.status(200).send(data);
    }else{
        res.status(400).send("Không tìm thấy user!")
    }
}
const getOrder = async(req,res) => {
    let data = await model.user.findAll({
        include: ["food_id_foods"]
    })
    if(data){
        res.status(200).send(data);
    }else{
        res.status(400).send("Không tìm thấy dữ liệu");
    }
}
const getOrderUserId = async (req,res) => {
    let id = req.params.id;
    let data = await model.user.findOne({
        where: {
            user_id : id,
        },
        include: ["food_id_foods"]
    })
    if (data){
        res.status(200).send(data);
    }else{
        res.status(400).send("Không tìm thấy user!")
    }
}
const orderFood = async (req,res) => {
    let {user_id,food_id,amount,code,arr_sub_id} = req.body;
    let index = await model.order.findOne({
        where: {
            food_id: food_id,
            user_id: user_id
        }
    });
    if(index !== null){
        res.status(400).send("Bạn đã đặt món rồi, vui lòng chỉnh sửa đơn hàng!")
    }
    else{
        await model.order.create({
            user_id,
            food_id,
            amount,
            code,
            arr_sub_id
        })
        res.status(200).send("Đặt món thành công!")
    }
        
}
const updateOrderFood = async(req,res) => {
    let {user_id,food_id,amount,code,arr_sub_id} = req.body;
    let index = await model.order.findOne({
        where: {
            food_id: food_id,
            user_id: user_id
        }
    });
    if(index == null){
        res.status(400).send("Vui lòng đặt món trước khi chỉnh sửa!")
    }
    else{
        await model.order.update({amount,code,arr_sub_id},{
            where:{
                food_id: food_id,
                user_id: user_id
            }
        })
        res.status(200).send("Sửa đơn hàng thành công!")
    }
}
const deleteOrderFood = async(req,res) => {
    let {user_id,food_id} = req.body;
    let index = await model.order.findOne({
        where: {
            food_id: food_id,
            user_id: user_id
        }
    });
    if(index == null){
        res.status(400).send("Không tìm thấy đơn hàng!")
    }
    else{
        await model.order.destroy({
            where:{
                food_id: food_id,
                user_id: user_id
            }
        })
        res.status(200).send("Xoá đơn hàng thành công!")
    }
}
module.exports = {getLikeUser,getLikeUserId,getRateUser,getRateUserId,getOrder,getOrderUserId,orderFood,updateOrderFood,deleteOrderFood}