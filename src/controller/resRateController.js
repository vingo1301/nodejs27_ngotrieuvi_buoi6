const initModels = require("../models/init-models")
const sequelize = require("../models/index")
const model = initModels(sequelize);

const getRateRes = async (req,res) => {
    let data = await model.restaurant.findAll({
        include: ["user_id_user_rate_res"],
    })
    if(data){
        res.status(200).send(data);
    }else{
        res.status(400).send("Không tìm thấy dữ liệu");
    }
}
const getRateResId = async (req,res) => {
    let id = req.params.id;
    let data = await model.restaurant.findOne({
        where: {
            res_id : id,
        },
        include: ["user_id_user_rate_res"]
    })
    if (data){
        res.status(200).send(data);
    }else{
        res.status(400).send("Không tìm thấy nhà hàng!")
    }
}
const postRateRes = async (req,res) => {
    const data = req.body;
    let index = await model.rate_res.findOne({
        where: {
            res_id: data.res_id,
            user_id: data.user_id
        }
    });
    if(index !== null){
        res.status(400).send("Bạn đã đánh giá nhà hàng rồi!")
    }
    else{
        await model.rate_res.create({
            res_id: data.res_id,
            user_id: data.user_id,
            amount: data.amount,
            date_rate: data.dateRate
        })
        res.status(200).send("Đánh giá nhà hàng thành công!")
    }
}
const deleteRate = async (req,res) => {
    const data = req.body;
    let index = await model.rate_res.findOne({
        where: {
            res_id: data.res_id,
            user_id: data.user_id
        }
    });
    console.log(index);
    if(index == null){
        res.status(400).send("ID user hoặc nhà hàng không chính xác!")
    }else{
        await model.rate_res.destroy({
            where: {
                res_id: data.res_id,
                user_id: data.user_id
            }
        })
        res.status(200).send("Xoá Đánh Giá Thành Công!")
    }
}
const updateRateRes = async (req,res) => {
    const data = req.body;
    let index = await model.rate_res.findOne({
        where: {
            res_id: data.res_id,
            user_id: data.user_id
        }
    });
    console.log(index);
    if(index == null){
        res.status(400).send("Bạn chưa đánh giá nhà hàng này!")
    }else{
        await model.rate_res.update(data,{
            where: {
                res_id: data.res_id,
                user_id: data.user_id
            }
        })
        res.status(200).send("Thay đổi đánh giá thành công!")
    }
}
module.exports = {getRateRes,getRateResId,postRateRes,deleteRate,updateRateRes}