const express = require("express");
const { resRoute } = require("./resRoute");
const userRoute = require("./userRoute");
const rootRoute = express.Router();

rootRoute.use("/res",resRoute);
rootRoute.use("/user",userRoute)
module.exports = rootRoute;