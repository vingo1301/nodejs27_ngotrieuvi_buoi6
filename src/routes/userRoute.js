const express = require("express");
const { getLikeUser, getLikeUserId, getRateUser, getRateUserId, getOrder, getOrderUserId, orderFood, updateOrderFood, deleteOrderFood } = require("../controller/userController");
const userRoute = express.Router();

userRoute.get("/getLikeUser",getLikeUser);
userRoute.get("/getLikeUserId/:id",getLikeUserId);
userRoute.get("/getRateUser",getRateUser);
userRoute.get("/getRateUserId/:id",getRateUserId);
userRoute.get("/getOrder",getOrder);
userRoute.get("/getOrderUserId/:id",getOrderUserId);
userRoute.post("/orderFood",orderFood);
userRoute.put("/updateOrderFood",updateOrderFood);
userRoute.delete("/deleteOrderFood",deleteOrderFood)

module.exports = userRoute