const express = require("express");
const { getLikeRes, getLikeResId, postLikeRes, unLikeRes } = require("../controller/resLikeController");
const { getRateRes, getRateResId, postRateRes, deleteRate, updateRateRes } = require("../controller/resRateController");
const resRoute = express.Router();

// like
resRoute.get("/getLike",getLikeRes);
resRoute.get("/getLikeId/:id",getLikeResId);
resRoute.post("/likeRes",postLikeRes);
resRoute.delete("/unlikeRes",unLikeRes)

// rate
resRoute.get("/getRate",getRateRes);
resRoute.get("/getRateId/:id",getRateResId);
resRoute.post("/rateRes",postRateRes);
resRoute.delete("/delRateRes",deleteRate)
resRoute.put("/updateRateRes",updateRateRes)

module.exports = {resRoute};